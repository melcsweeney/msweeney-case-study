import React, { Component } from 'react';
import {Row, Col, Button, ButtonToolbar} from 'react-bootstrap';
import { connect } from 'react-redux';


class ProductForm extends Component {

	render () {

		let findInStore = '';
		if (this.props.info.findInStore) {
			findInStore = (
				<Col xs={6} className="ProductButtons-findStore">
					<Button className="btn btn-secondary u-ttu u-shadow" block>Pickup in store</Button>
					<Button bsStyle="link" className="u-hide--xs">find in a store</Button>
				</Col>
			)
		}

		let addToCartBtn = '';
		if (this.props.info.availOnline) {
			addToCartBtn = (
				<Col xs={6}>
					<Button bsStyle="primary" className="u-ttu u-shadow" block>Add to cart</Button>
				</Col>
			)
		}

		return (
			<form>
				{/*Quantity selector*/}
				<Row>
					<Col xs={6} sm={12} md={6}>
						<div className="ProductQuantity">
							quantity:
							<button type="button" 
								className="buttonPlus pull-right" 
								onClick={this.props.onIncrementCounter}
								aria-label="Increase product quantity">
							</button>
							<label htmlFor="orderQty" className="u-visuallyhidden">Quantity:</label>
							<input id="orderQty" 
								type="number"
								className="no-style no-spinner"
								placeholder={this.props.ctr} value={this.props.ctr}
								readOnly/>
							<button type="button"
								className="buttonMinus pull-right"
								onClick={this.props.onDecrementCounter}
								aria-label="Decrease product quantity">
							</button> 
						</div>
					</Col>
				</Row>
							
				{/*Action buttons*/}
				<Row className="ProductButtons">
					{findInStore}
					{addToCartBtn}
				</Row>

				{/*Returns info*/}
				<Row className="ProductReturns">
					<Col className="ProductReturns-title" xs={3} sm={3} md={2}>
						returns:
					</Col>
					<Col className="ProductReturns-detail" xs={9} sm={8} md={9}>
						<p>This item must be returned within 30 days of the ship date. See return policy for details. Prices, promotions, styles and availability may vary by store and online.</p>
					</Col>
				</Row>

				{/*Secondary buttons*/}
				<Row >
					<ButtonToolbar className="ProductButtons-secondary">
						<Col xs={4}>
							<Button bsSize="small" className="u-ttu" block>Add to registry</Button>
						</Col>
						<Col xs={4}>
							<Button bsSize="small" className="u-ttu" block>Add to list</Button>
						</Col>
						<Col xs={4}>
							<Button bsSize="small" className="u-ttu" block>Share</Button>
						</Col>
					</ButtonToolbar>
				</Row>
			</form>
		);
	}

}

const mapStateToProps = state => {
	return {
		ctr: state.counter,
	};
}

const mapDispatchToProps = dispatch => {
	return {
		onIncrementCounter: () => dispatch({type: 'INCREMENT'}),
		onDecrementCounter: () => dispatch({type: 'DECREMENT'})
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductForm);