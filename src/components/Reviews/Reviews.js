import React from 'react';
import {Row, Col} from 'react-bootstrap';
import star1 from '../../assets/images/1star.png'; 
import star5 from '../../assets/images/5star.png';

const reviews = () => {
	return (
		<aside>
			<Col xs={12}>
				<Row className="ProductReviews-overall">
					<Col xs={12} sm={8}>
						<img src={star5} alt="overall: 5 out of 5 stars, see all reviews"/>overall
					</Col>
					<Col xs={12} sm={4}>
						view all 14 reviews
					</Col>
				</Row>
			</Col>
			<Col xs={12}>
				<Row className="ProductReviews-proCon">
					{/*Pro Con header*/}
					<Col xs={6}>
						<div className="ProductReviews-proConTitle u-ttu">Pro</div>
						<div className="ProductReviews-proCondescription">most helpful 4-5 star review</div>
					</Col>
					<Col xs={6}>
						<div className="ProductReviews-proConTitle u-ttu">Con</div>
						<div className="ProductReviews-proCondescription">most helpful 1-2 star review</div>
					</Col>
					<Col xs={12}>
						<hr/>
					</Col>
					<Col className="ProductReviews-review" xs={6}>
						<article>
							<img src={star5} alt="5 out of 5 stars"/>
							<div className="ProductReviews-reviewTitle">Fantastic Blender</div>
							<p>This blender works amazingly, and blends within seconds. The single serve cups also work really well for smoothies or protein shakes!</p>
							<p><a href="#eric">Eric</a> April 18, 2013</p>
						</article>
					</Col>
					<Col className="ProductReviews-review" xs={6}>
						<article>
							<img src={star1} alt="1 out of 5 stars"/>
							<div className="ProductReviews-reviewTitle">Very unhappy</div>
							<p>Less than 2 months after purchase it completely stopped working. First it wouldn't detect the pitcher when trying to blend a significant amount, a couple weeks later it wouldn't detect the single serve cup.</p>
							<p><a href="#newYork">New York</a> March 11, 2013 </p>
						</article>
					</Col>
				</Row>
			</Col>
		</aside>

	);
}

export default reviews;