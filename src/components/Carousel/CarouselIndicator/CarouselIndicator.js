import React from 'react';


const carouselIndicator = (props) => (
	<li className={props.class} onClick={props.click}>
		 <img src={require('../../../assets/images/' + props.img)} alt=""/>
	</li>
);

export default carouselIndicator;