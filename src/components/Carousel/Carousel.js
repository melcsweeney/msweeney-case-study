/*---------------------------------------------------------------------------------*/
/*  This carousel was built with help from the following tutorial
/* 	https://blog.alexdevero.com/create-simple-carousel-react-js/
/*---------------------------------------------------------------------------------*/

import React, { Component } from 'react';
import {Row, Col} from 'react-bootstrap';
import CarouselImage from './CarouselImage/CarouselImage';
import CarouselIndicator from './CarouselIndicator/CarouselIndicator';
import magnify from '../../assets/images/magnify.png';



let imageSlider = '';
let imageThumbs = '';

class Carousel extends Component {
	state = {
		activeIndex: 0,
		images: ''
	}

	goToSlide(index) {
		this.setState({activeIndex: index});
	}

	goToPrevSlide(e) {
		e.preventDefault();
		let index = this.state.activeIndex;
		let slides  = imageSlider;
		let slidesLength = slides.length;
		if ( index < 1) {
			index = slidesLength;
		}
		--index;
		this.setState({activeIndex: index});
	}

	goToNextSlide(e) {
		e.preventDefault();
		let index = this.state.activeIndex;
		let slides = imageSlider;
		let slidesLength = slides.length - 1;
		if ( index === slidesLength) {
			index = -1;
		}
		++index;
		this.setState({activeIndex: index});
	}

	render() {

		if(this.props.images) {
			imageSlider = this.props.images.map((image, index) => {
				let imagePath = image+'.jpg';
				return (
					<CarouselImage 
						key={index}
						class={index === this.state.activeIndex ? "Carousel-slide Carousel-slide--active" : "Carousel-slide"} 
						img={imagePath}/>
				);
			});
			imageThumbs = this.props.images.map((image, index) => {
				let imagePath = image+'_thumb.jpg';
				return (
					<CarouselIndicator 
						key={index}
						class={index === this.state.activeIndex ? "Carousel-thumbs Carousel-thumbs--active" : "Carousel-thumbs"} 
						img={imagePath}
						click={e => this.goToSlide(index)}/>
				);
			});
		}

		return (
			
			<Row className="Carousel">
				<Col xs={12}>
					<ul className="Carousel-slides">
						{imageSlider}
					</ul>
				</Col>

				<Row className="u-hide--xs">
					<Col xs={12} sm={8} className="col-sm-offset-2">
						<button className="Carousel-magnify" type="button">
							<img src={magnify} alt=""/>view larger
						</button>
					</Col>
				</Row>

				<Row>
					<Col xs={12} sm={8} className="col-sm-offset-2">
						<button type="button"
							className="Carousel-arrow Carousel-arrow--right" 
							onClick={e => this.goToNextSlide(e)}
							aria-label="Advance image carousel"/>
						<ul className="Carousel-thumbs">
							{imageThumbs}
						</ul>
						<button type="button"
							className="Carousel-arrow Carousel-arrow--left" 
							onClick={e => this.goToPrevSlide(e)}
							aria-label="Previous carousel image"/>
					</Col>
				</Row>

			</Row>

		);
	}
}

export default Carousel;