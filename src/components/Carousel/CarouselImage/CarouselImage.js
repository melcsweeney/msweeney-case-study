import React from 'react';


const carouselImage = (props) => (
	<li className={props.class}>
		 <img src={require('../../../assets/images/' + props.img)} alt=""/>
	</li>
);

export default carouselImage;