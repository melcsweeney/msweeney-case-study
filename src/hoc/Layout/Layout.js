import React, { Component } from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import axios from 'axios';
import ProductForm from '../../components/ProductForm/ProductForm';
import Carousel from '../../components/Carousel/Carousel';
import Reviews from '../../components/Reviews/Reviews';


class Layout extends Component {

	state = {
		product: {
			title: '',
			price: '',
			availOnline: false,
			findInStore: false,
		}
	}

	componentDidMount () {
		axios.get('item-data.json')
			.then(response => {
				
				const productData = response.data.CatalogEntryView[0];

				let price = '';
				productData.Offers.map(offer => {
					offer.OfferPrice.map(offerPrice => {
						price = offerPrice.formattedPriceValue;
						return price;
					})
				})

				//Load up all pof the product images
				let prodImages = [];
				for (let image of productData.Images[0].AlternateImages) {
					let imageName = image.image;
					let i = imageName.lastIndexOf('/');
					imageName = imageName.slice(i+1) //clean up the image name so it is served locally
					prodImages.push(imageName);
				}
				prodImages = prodImages.slice(0,2);

				//Get the primary product image and add it to the first position in the product images array
				let primaryImage = productData.Images[0].PrimaryImage[0].image;
				let j = primaryImage.lastIndexOf('/');
				primaryImage = primaryImage.slice(j+1); //clean up the image name so it is served locally
				prodImages.unshift(primaryImage);

				const updatedProduct = {
					...this.state.product,
					price: price,
					title: productData.title,
					images: prodImages,
					findInStore: +productData.purchasingChannelCode === 0 || +productData.purchasingChannelCode === 2 ? true : false,
					availOnline: +productData.purchasingChannelCode === 0 || +productData.purchasingChannelCode === 1 ? true : false
				};

				this.setState({product: updatedProduct});
			})
			.catch(error => {
				console.log(error);
			}); 
	}

	render () {
		return (
			<main>
				<Grid>
					<Row>

						{/*Left col*/}
						<section>
							<Col sm={6}>
								<Col className="text-center" md={12}>
									<h1 className="Product-title">{this.state.product.title}</h1>
								</Col>
								{/*Carousel*/}
								<Col className="text-center" md={12}>
									<Carousel images={this.state.product.images}/>
								</Col>
							</Col>
						</section>

						{/*Right col*/}
						<section>
							<Col sm={6}>
								<Col className="ProductPrice u-noPadding" md={12}>
									<span className="ProductPrice-price">{this.state.product.price}</span> <span className="ProductPrice-detail">online price</span>
								</Col>
								<Col className="ProductPromotions u-noPadding" md={12}>
									<ul>
										<li>spend $50, ship free</li>
										<li>$25 gift card with purchase of a select Ninja Blender</li>
									</ul>
								</Col>
								{/*Product form*/}
								<ProductForm info={this.state.product}/>
								{/*Product Highlights*/}
								<Row className="ProductHighlights">
									<Col xs={12}>
										<h2 className="ProductHighlights-title u-ttl">Proudct hightlights</h2>
										<ul className="ProductHighlights-list">
											<li>Wattage Output: 1100 Watts</li>
											<li>Number of Speeds: 3</li>
											<li>Capacity (volume): 72.0 Oz.</li>
											<li>Appliance Capabilities: Blends</li>
											<li>Includes: Travel Lid</li>
											<li>Material: Plastic</li>
											<li>Finish: Painted</li>
											<li>Metal Finish: Chrome</li>
											<li>Safety and Security Features: Non-Slip Base</li>
											<li>Care and Cleaning: Easy-To-Clean, Dishwasher Safe Parts</li>
										</ul>
									</Col>
								</Row>
							</Col>
						</section>
					</Row>
					{/* Second row */}
					<Row>
						<Col className="ProductReviews" md={6}>
							<Reviews />
						</Col>
					</Row>
				</Grid>
			</main>
		);
	}
}


export default Layout;