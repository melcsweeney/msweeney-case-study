const initialState = {
	counter: 1
}

const reducer = (state = initialState, action) => {
	switch(action.type) {
		case 'INCREMENT':
			return {
				counter: state.counter + 1
			}
		case 'DECREMENT':
			return {
				counter: state.counter === 0 ? 0 : state.counter -1
			}
		default:
			return state;	
	}
}

export default reducer;